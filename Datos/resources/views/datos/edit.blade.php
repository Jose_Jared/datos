@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 style="text-align: center">Datos Personales</h1>
                <form action="{{url(route('datos.update',$dato->id))}}" method="post">
                    {{method_field('PATCH')}}
                    @csrf
                    <div class="form-group">
                        <label for="">Nombre</label>
                        <input value="{{$dato->nombre}}" name="nombre" type="text" class="form-control" id="nombre">
                    </div>
                    <div class="form-group">
                        <label for="">Apellido Paterno</label>
                        <input value="{{$dato->apellidop}}" name="apellidop" type="text" class="form-control" id="apellidop">
                    </div>
                    <div class="form-group">
                        <label for="">Apellido Materno</label>
                        <input value="{{$dato->apellidom}}" name="apellidom" type="text" class="form-control" id="apellidom">
                    </div>
                    <div class="form-group">
                        <label for="">Fecha de Nacimiento</label>
                        <input value="{{$dato->fecha}}" name="fecha" type="date" class="form-control" id="fecha">
                    </div>
                    <input type="submit" class="btn btn-primary" value="Guardar">
                </form>
            </div>
        </div>
    </div>
@endsection

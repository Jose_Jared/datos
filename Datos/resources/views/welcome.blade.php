<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
    @extends('layouts.app')
    @section('content')
        <br><br>
        <h1 class="flex-center">Practica laravel</h1>
        <br><br><br><br><br><br>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Nuevo</h5>
                            <hr>
                            <p class="card-text">Agregar un nuevo registro a la Base de Datos.</p>
                            <form action="{{route('datos.create')}}" method="get">
                                <input type="submit" class="btn btn-success" value="Nuevo">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Registros</h5>
                            <hr>
                            <p class="card-text">Lista con los registro existentes en la Base de Datos.</p>
                            <form action="{{route('datos.index')}}">
                                <input type="submit" class="btn btn-success" value="Registro">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Editar</h5>
                            <hr>
                            <p class="card-text">Edita algun registro de la Base de Datos.</p>
                            <form action="{{route('datos.index')}}">
                                <input type="submit" class="btn btn-success" value="Editar">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Eliminar</h5>
                            <hr>
                            <p class="card-text">Elimina algun registro de la Base de Datos.</p>
                            <form action="{{route('datos.index')}}">
                                <input type="submit" class="btn btn-success" value="Eliminar">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        </div>
    </body>
</html>
